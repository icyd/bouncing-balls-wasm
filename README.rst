Bouncing balls Wasm
===================

Install express server to see the project and run it as:

.. code-block:: bash

   npm install
   npm start

Compiling code
--------------

To compile code you need to install `Emscripten`_ and run from root directory:

.. code-block:: bash

   emcc -Ilib lib/circles.c lib/canvas.c -s WASM=1 -s EXPORTED_FUNCTIONS="['_main','_update_circles']" --pre-js lib/pre-js.js -o public/canvas.js

.. _Emscripten: https://emscripten.org/index.html

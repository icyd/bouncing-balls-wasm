#include <stdlib.h>
#include "circles.h"

static Circle circles[NUM_CIRCLES];

int main(int argc, char **argv)
{
    if (argc == 3) {
        int width = atoi(argv[1]);
        int height = atoi(argv[2]);
        init_circles(circles, width, height);

        EM_ASM({render($0, $1, $2);}, circles, NUM_CIRCLES*sizeof(Circle)/sizeof(int),
            sizeof(Circle)/sizeof(int));
    }
    return 1;
}

#ifndef CIRCLES_H_FHIM5SVE
#define CIRCLES_H_FHIM5SVE

#include <stdlib.h>
#include <time.h>
#include <emscripten.h>

#define NUM_CIRCLES 100
#define MAX_RADIUS 40
#define MAX_VELOCITY 15
#define get_rand(max) (rand() % (max))

typedef struct Circle {
    unsigned int x;
    unsigned int y;
    unsigned int r;
    union {
        unsigned int rgb;
        struct {
            unsigned char blue;
            unsigned char green;
            unsigned char red;
        };
    };
    int vel_x;
    int vel_y;
} Circle;

void init_circles(Circle *circles, unsigned int max_x, unsigned int max_y);
static void update_coordinates(Circle *circle, unsigned int max_x, unsigned int max_y);
void *get_circles(Circle *circles, unsigned int max_x, unsigned int max_y);
#endif /* end of include guard: CIRCLES_H_FHIM5SVE */

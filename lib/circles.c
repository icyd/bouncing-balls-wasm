#include "circles.h"
#include <stdio.h>

void init_circles(Circle *circles, unsigned int max_x, unsigned int max_y)
{
    srand(time(NULL));

    for (int i=0; i<NUM_CIRCLES; i++) {
        int radius = get_rand(MAX_RADIUS);
        circles[i].x = get_rand(max_x - 2 * radius) + radius;
        circles[i].y = get_rand(max_y - 2 * radius) +  radius;
        circles[i].r = radius;
        circles[i].red = get_rand(255);
        circles[i].green = get_rand(255);
        circles[i].blue = get_rand(255);
        circles[i].vel_x = get_rand(MAX_VELOCITY) - MAX_VELOCITY / 2;
        circles[i].vel_y = get_rand(MAX_VELOCITY) -MAX_VELOCITY / 2;
    }
}

static void update_coordinates(Circle *circle, unsigned int max_x, unsigned int max_y)
{
    circle->x += circle->vel_x;
    circle->y += circle->vel_y;
    if ((circle->x + circle->r) > max_x) {
        circle->x = max_x - circle->r;
        circle->vel_x *= -1;
    } else if ((int)(circle->x - circle->r) < 0) {
        circle->x = circle->r;
        circle->vel_x *= -1;
    }

    if ((circle->y + circle->r) > max_y) {
        circle->y = max_y - circle->r;
        circle->vel_y *= -1;
    } else if ((int)(circle->y - circle->r) < 0) {
        circle->y = circle->r;
        circle->vel_y *= -1;
    }
}

void update_circles(Circle *circles, unsigned int max_x, unsigned int max_y)
{
    for (int i=0; i<NUM_CIRCLES; i++) {
        update_coordinates(&circles[i], max_x, max_y);
    }
}
